% Define a starting folder wherever you want
folder{1} = 'data/tech/';
folder{2} = 'data/sport/';
folder{3} = 'data/politics/';
folder{4} = 'data/entertainment/';
folder{5} = 'data/business/';

nClass = size(folder,2);

label = [];
nSample = [];
docs =[];

for clNo = 1:5
    %files = dir(folder{folderNo})
    files = dir(strcat(folder{clNo},'*.txt'));
    nSample(clNo) = size(files,1);
    for fileNo = 1: size(files,1)
        fileName = files(fileNo).name;
        filePath = strcat(folder{clNo},fileName);
        str = extractFileText(filePath);
        doc=regexprep(str,'[^a-zA-Z]',' ');   % remove special chars and punctuation marks
        docs = [docs; doc];
        label = [label; clNo];
       % save('label.mat','label');
    end
end

documents = tokenizedDocument(docs);
%Create a bag-of-words model using bagOfWords.
bag = bagOfWords(documents);
bag = removeWords(bag,stopWords);
tfidfMat = full(tfidf(bag));
% save('tfidfMat.mat','tfidfMat');

%pca
%  plot(tfidfMat)
tfeat = pca(tfidfMat);
%plot(tfeat);
load('hist_10neighbor_32.mat');
feat=[tfidfMat*tfeat(:,1:32) hist1];
%feat=[tfidfMat*tfeat(:,1:64) normalize(hist1)];

%feat=tfidfMat;
%S2=load('z')
%feat= S2.z;

%Select half of the min and half of each class
%trainRatio = 0.50;
%minSample = 386;
%minTrainSample  = ceil(minSample*trainRatio);
%trainInd = [];
%testInd=[];
%for clNo = 1:nClass
% clIndx = find(label==clNo);
%trInd = clIndx(1:minTrainSample) %to select half of the minimum
%  trInd = clIndx(1:2:end); % to select half of each class
%   trainInd = [trainInd; trInd];
%end

%temp = zeros(size(label,1),1);
%temp(trainInd,:)=1;
%testInd = find(temp==0);

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Manually taking data for TRAINING and TESTING
%trainInd =[1:193 402:595 912:912+193    1298:1298+193   1808:1808+190]';
%testInd =[194:401 596:911 1106:1297 1491:1807 1999:2225]';
%trainInd =[1:311 402:713 915:915+311 1332:1332+311   1720:1720+311]';
%testInd =[312:401 714:914 1227:1332-1 1644:1719 2031:2225]';

%%%%%%%%%%%%%%%%%%%%%%%%%

%Randomly take data for TRAINING and TESTING OVERALL
%num_points = 2225;
%prompt = 'What is the percentage for training and testing? ';
%percent = input(prompt);
%split_point = round(num_points*percent);
%seq = randperm(num_points)
%trainInd = seq(1:split_point);
%testInd = seq(split_point+1:end);

%%%%%%%%%%%%%%%%%&&&&&&&&&
%Randomly take data from each class

%trainInd= [];
%testInd= [];

%for clNo = 1:5
    %files = dir(strcat(folder{clNo},'*.txt'));
    %clIndx = find(label==clNo);
    %split_point = round(size(files,1)*0.5);
    %seq = randperm(size(files,1));
    %x =seq(1:split_point)';
    %y = seq(split_point+1:end)';
    
    %trInd= clIndx(x);
    %ttInd= clIndx(y);
    
    %trainInd=[trainInd;trInd];
   % save('trainInd.mat','trainInd');
  %  testInd=[testInd;ttInd];
 %   save('testInd.mat','testInd');
    
%end

%%%%%%%%%%%%%%%%%%%%%%%%
load('trainInd.mat');
load('testInd.mat');
trainFeat = feat(trainInd,:);
%save('trainFeat.mat','trainFeat');
trainLabel = label(trainInd,:);
%save('trainLabel.mat','trainLabel');

testFeat = feat(testInd,:);
%save('testFeat.mat','testFeat');
testLabel = label(testInd,:);
%save('testLabel.mat','testLabel');

t = templateSVM('Standardize',false,'SaveSupportVectors',true, 'KernelScale','auto', 'BoxConstraint', 26);
%t = templateSVM('Standardize',false,'SaveSupportVectors',true, 'KernelScale','auto','KernelFunction','gaussian', 'KernelOffset',1, 'BoxConstraint', 1000);

svmModel = fitcecoc(trainFeat, trainLabel,'Learners',t);
predLabel = predict(svmModel, testFeat);
OA = sum(predLabel==testLabel)*100/size(testLabel,1)

classAccuracy = [];
for c = 1:nClass
    pred = predLabel(testLabel==c);
    test = testLabel(find(testLabel==c));
    OA = sum(pred == test)*100/size(test, 1);
    classAccuracy = [classAccuracy; c OA];
end
classAccuracy;

% %fprintf('Train')
% numOftrain1=nnz(trainLabel==1)
% numoftrain2=nnz(trainLabel==2)
% numOftrain3=nnz(trainLabel==3)
% numOftrain4=nnz(trainLabel==4)
% numOftrain5=nnz(trainLabel==5)
% 
% %fprintf('Predict')
% numOfPredict1=nnz(predLabel==1)
% numofPredict2=nnz(predLabel==2)
% numOfPredict3=nnz(predLabel==3)
% numOfPredict4=nnz(predLabel==4)
% numOfPredict5=nnz(predLabel==5)
% 
% %fprintf('Test')
% numOfTest1=nnz(testLabel==1)
% numofTest2=nnz(testLabel==2)
% numOfTest3=nnz(testLabel==3)
% numOfTest4=nnz(testLabel==4)
% numOfTest5=nnz(testLabel==5)
% 
% tp = sum((predLabel == 1) & (testLabel == 1))
% fp = sum((predLabel == 1) & (testLabel == 0))
% fn = sum((predLabel == 0) & (testLabel == 1))
% 
% precision = tp / (tp + fp)
% recall = tp / (tp + fn)
% F1 = (2 * precision * recall) / (precision + recall)
