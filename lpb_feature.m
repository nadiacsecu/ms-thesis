% Define a starting folder wherever you want
folder{1} = 'output/tech/';
folder{2} = 'output/sport/';
folder{3} = 'output/politics/';
folder{4} = 'output/entertainment/';
folder{5} = 'output/business/';

descriptors = [];
label = [];
nSample = [];
% newStr =[];

decList=[];
docs =[];
hist1=[];

dec=[];
for clNo = 1:5
    %files = dir(folder{folderNo})
    files = dir(strcat(folder{clNo},'*.txt'));
    nSample(clNo) = size(files,1);
    for fileNo = 1: size(files,1)
        fileName = files(fileNo).name;
        filePath = strcat(folder{clNo},fileName);
        str = extractFileText(filePath);
        doc=regexprep(str,'[^a-zA-Z]',' ');   % remove special chars and punctuation marks
        docs = [docs; doc];
        label = [label; clNo];
    end
end

documents = tokenizedDocument(docs);
out = [];

%	LBP Blocks for neighbor 8

%for r=1:size(documents,1)
%   out =documents(r).Vocabulary;
%  for j=5:size(out,2)-5+1                                                 %compare using lexicographical order
%     temp = [];
%    temp(1)=strcmpc(out{j},out{j-4});
%   temp(2)=strcmpc(out{j},out{j-3});
%  temp(3)=strcmpc(out{j},out{j-2});
% temp(4)=strcmpc(out{j},out{j-1});
%temp(5)=strcmpc(out{j},out{j+1});
%temp(6)=strcmpc(out{j},out{j+2});
%temp(7)=strcmpc(out{j},out{j+3});
%temp(8)=strcmpc(out{j},out{j+4});
%tempint = 0;

%for k= 1:8
%   tempint = tempint+temp(k)*2^(8-k);   %binary to decimal
%end
%dec(j) = tempint;
%end
%dec = dec(5:end);
%r=r+1;
%fileHist = zeros(1,16);
%for k = 1:length(dec)
%   indx = fix(dec(k)/16) + 1;
%  fileHist(indx) = fileHist(indx) + 1;              %increment count++
%end
%hist1 = [hist1; fileHist];
%end

%   LBP Blocks for neighbor 10

%for r=1:size(documents,1)
 %   out =documents(r).Vocabulary
  %  for j=6:size(out,2)-6+1            %compare using lexicographical order
   %     temp = [];
        
    %    temp(1)=strcmpc(out{j},out{j-5});
     %   temp(2)=strcmpc(out{j},out{j-4});
      %  temp(3)=strcmpc(out{j},out{j-3});
       % temp(4)=strcmpc(out{j},out{j-2});
        %temp(5)=strcmpc(out{j},out{j-1});
        %temp(6)=strcmpc(out{j},out{j+1});
        %temp(7)=strcmpc(out{j},out{j+2});
        %temp(8)=strcmpc(out{j},out{j+3});
        %temp(9)=strcmpc(out{j},out{j+4});
        %temp(10)=strcmpc(out{j},out{j+5});
        
        %tempint = 0;
        %for k= 1:10
         %   tempint = tempint+temp(k)*2^(10-k);   %binary to decimal
        %end
        %dec(j) = tempint;
    %end
    %dec;
   % decList = [decList;dec];
    %r=r+1;
    
    %   Histogram Block for neighbor 10
    %fileHist = zeros(1,64);
    %for k = 1:length(dec)
    %    indx = fix(dec(k)/16) + 1;
     %   fileHist(indx) = fileHist(indx) + 1;              %increment count++
    %end
    %hist1 = [hist1; fileHist];
%end

% LBP blocks for neighbor 12

for r=1:size(documents,1)
    out =documents(r).Vocabulary
    for j=7:size(out,2)-7+1            %compare using lexicographical order
        temp = [];
        
        temp(1)=strcmpc(out{j},out{j-6});
        temp(2)=strcmpc(out{j},out{j-5});
        temp(3)=strcmpc(out{j},out{j-4});
        temp(4)=strcmpc(out{j},out{j-3});
        temp(5)=strcmpc(out{j},out{j-2});
        temp(6)=strcmpc(out{j},out{j-1});
        temp(7)=strcmpc(out{j},out{j+1});
        temp(8)=strcmpc(out{j},out{j+2});
        temp(9)=strcmpc(out{j},out{j+3});
        temp(10)=strcmpc(out{j},out{j+4});
        temp(11)=strcmpc(out{j},out{j+5});
        temp(12)=strcmpc(out{j},out{j+6});
        tempint = 0;
        for k= 1:12
            tempint = tempint+temp(k)*2^(12-k);   %binary to decimal
        end
        dec(j) = tempint;
    end
    dec;
   % decList = [decList;dec];
    r=r+1;
    
    %   Histogram Block for neighbor 12
    fileHist = zeros(1,128);
    for k = 1:length(dec)
        indx = fix(dec(k)/32) + 1;
        fileHist(indx) = fileHist(indx) + 1;              %increment count++
    end
    hist1 = [hist1; fileHist];
end


save('hist1.mat','hist1');
%label = [label; folderNo];



